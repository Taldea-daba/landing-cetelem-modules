import {
    saveAs
} from 'file-saver';
import axios from 'axios';

const isLocalEnvironment = () => {
    return (
        window.location.hostname === "localhost" ||
        window.location.hostname === "127.0.0.1"
    );
};

export function downLoadLatestReleasePrestashop(gitLink) {
    // URL de tu servidor PHP que actúa como proxy
    const proxyUrl = isLocalEnvironment() ?
        "http://localhost/send-lead-cetelem/?action=downloadZip" :
        "https://leadsender.dabasystem.com/?action=downloadZip";

    // Crear un objeto FormData para enviar los datos en formato compatible con PHP
    const formData = new FormData();
    formData.append('gitLink', gitLink);

    // Hacer una solicitud POST al proxy con FormData
    axios.post(proxyUrl, formData, {
            responseType: 'blob'
        })
        .then(response => {
            if (response.status !== 200) {
                throw new Error('Error al descargar el archivo ZIP');
            }

            // Crear un blob del archivo ZIP modificado
            const zipBlob = new Blob([response.data], {
                type: 'application/zip'
            });

            // Descargar el archivo ZIP modificado
            const fileName = 'cetelem.zip';
            saveAs(zipBlob, fileName);
        })
        .catch(error => {
            console.error('Error al descargar el archivo ZIP:', error);
        });
}


export function downLoadLatestRelease(gitLink) {
    var repoUrl = `https://api.github.com/repos/${gitLink}/releases/latest`;

    // Hacer una solicitud GET a la API de GitHub
    fetch(repoUrl)
        .then(response => response.json())
        .then(data => {
            // Obtener la versión de la release
            var releaseVersion = data.tag_name;

            // Construir la URL de descarga dinámicamente
            var downloadUrl = `https://github.com/${gitLink}/archive/refs/tags/${releaseVersion}.zip`;

            // Redirigir a la URL de descarga   
            window.location.href = downloadUrl;

            // Manipular la respuesta para obtener la información que deseas
            // var tagName = data.tag_name;
            // var releaseName = data.name;
            // var releaseUrl = data.html_url;

            // Construir el mensaje para mostrar en la página
            // var message = `La última release es ${tagName} (${releaseName}). Puedes encontrar más información <a href="${releaseUrl}">aquí</a>.`;

            // Mostrar el mensaje en la página
            // document.getElementById('latest-release-info').innerHTML = message;
        })
        .catch(error => {
            console.error('Error al obtener la última release:', error);
            // document.getElementById('latest-release-info').innerHTML = 'Error al obtener la información.';
        });

}

export function scrollToSection(sectionId) {
    const section = document.getElementById(sectionId);
    if (section) {
        section.scrollIntoView({
            behavior: "smooth"
        });
    }
}