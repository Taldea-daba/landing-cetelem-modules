import React from "react";
import ModalForm from "./ModalForm";

export const CtaSection = ({styles, sectionId, versions, titulo, texto, imagen, stylesSection }) => {

  return (
    <section id={sectionId} className={`${stylesSection} bg-white dark:bg-gray-900`}>
      <div className={`gap-8 py-40 px-4 flex flex-col xl:gap-16 lg:px-6 ${styles}`}>
        {/* <img
          className="w-full h-full dark:hidden"
          src={imagen}
          alt="dashboard image"
        />
        <img
          className="w-full h-full hidden dark:block"
          src={imagen}
          alt="dashboard image"
        /> */}
        <div className="mt-4">
          <h1 className="max-w-2xl mb-4 text-4xl font-extrabold tracking-tight leading-none md:text-5xl xl:text-6xl dark:text-white">
            {titulo}
          </h1>
          <p className="mb-6 font-light text-gray-500 md:text-lg dark:text-gray-400">
            {texto}
          </p>
          <div className="flex gap-4 flex-wrap">
            {versions?.map(({url, version, isUrl, isPdf}) => (
              <ModalForm key={version} version={version} gitUrl={url} isUrl={isUrl} isPdf={isPdf} />
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};
