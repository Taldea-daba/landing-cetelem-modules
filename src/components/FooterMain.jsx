import React from "react";
import { Footer } from "flowbite-react";

export const FooterMain = () => {
  return (
    <Footer container className="rounded-none">
      <div className="w-full text-center">
        <div className="w-full justify-between sm:flex sm:items-center sm:justify-between">
          <Footer.Brand
            target="blank"
            href="https://www.cetelem.es/"
            src="logo-cetelem.svg"
            alt="cetelem Logo"
          />
          <Footer.LinkGroup>
            <Footer.Link target="blank" href="https://www.cetelem.es/proteccion-de-datos">Política de privacidad</Footer.Link>
            <Footer.Link target="blank" href="https://www.cetelem.es/contacto">Contacto</Footer.Link>
          </Footer.LinkGroup>
        </div>
        <Footer.Divider />

        <a href="https://link.crmlab.es/rj7m3" target="_blank" rel="noopener noreferrer" className="text-sm text-gray-500 dark:text-gray-400 sm:text-center">
          © 2024 By Dabasystem
        </a>
      </div>
    </Footer>
  );
};
