import { Button, Navbar, DarkThemeToggle } from "flowbite-react";
import { scrollToSection } from "../utils"

export default function Header() {

  return (
    <Navbar fluid className="fixed z-20 w-full shadow-md dark:shadow-none">
      <Navbar.Brand target="blank" href="https://www.cetelem.es/">
        <img
          src="/logo-cetelem.svg"
          className="mr-3 h-6 sm:h-9"
          alt="Flowbite React Logo"
        />
      </Navbar.Brand>
      <div className="flex md:order-2">
        <DarkThemeToggle className="mr-2" />
        <Button color="primary">Solicitar ayuda</Button>
        <Navbar.Toggle />
      </div>
      <Navbar.Collapse className="sm:ml-14">
        <Navbar.Link href="javascript:" className="text-sm" onClick={() => scrollToSection('prestashop-section')}>Prestashop</Navbar.Link>
        <Navbar.Link href="javascript:" className="text-sm" onClick={() => scrollToSection('magento-section')}>Magento</Navbar.Link>
        <Navbar.Link href="javascript:" className="text-sm" onClick={() => scrollToSection('shopify-section')}>Shopify</Navbar.Link>
        <Navbar.Link href="javascript:" className="text-sm" onClick={() => scrollToSection('woocommerce-section')}>WooCommerce</Navbar.Link>
      </Navbar.Collapse>
    </Navbar>
  );
}
