import { Button, Modal } from "flowbite-react";
import { useState } from "react";
import { scrollToSection } from "../utils"

export const ModalHero = () => {
  const [openModal, setOpenModal] = useState(false);

  return (
    <>
      <Button color="primary" className="text-center px-5 py-3items-center" 
      onClick={() => setOpenModal(true)}>Descargas</Button>
      <Modal dismissible show={openModal} onClose={() => setOpenModal(false)}>
        <Modal.Header>Selecciona el modulo adecuado para tu Ecommerce</Modal.Header>
        <Modal.Body>
          <div className="space-y-2">
            <p className="font-bold text-lg leading-relaxed dark:text-gray-400">
            Prestashop
            </p>
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              With less than a month to go before the European Union enacts new
              consumer privacy laws for its citizens.
            </p>
            <Button color="primary" onClick={() => { setOpenModal(false); scrollToSection('prestashop-section'); }}>Descargar para PrestaShop</Button>
          </div>
        </Modal.Body>
        <Modal.Body>
          <div className="space-y-2">
            <p className="font-bold text-lg leading-relaxed dark:text-gray-400">
            Magento
            </p>
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              With less than a month to go before the European Union enacts new
              consumer privacy laws for its citizens.
            </p>
            <Button color="primary" onClick={() => { setOpenModal(false); scrollToSection('magento-section'); }}>Descargar para Magento</Button>
          </div>
        </Modal.Body>
        <Modal.Body>
          <div className="space-y-2">
            <p className="font-bold text-lg leading-relaxed dark:text-gray-400">
            Shopify
            </p>
            <p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
              With less than a month to go before the European Union enacts new
              consumer privacy laws for its citizens.
            </p>
            <Button color="primary" onClick={() => { setOpenModal(false); scrollToSection('shopify-section'); }}>Descargar para Shopify</Button>
          </div>
        </Modal.Body>

      </Modal>
    </>
  );
};
