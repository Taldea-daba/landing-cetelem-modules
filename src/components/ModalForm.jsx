import { useState } from "react";
import { Button, Checkbox, Label, Modal, TextInput } from "flowbite-react";
import {
  downLoadLatestRelease,
  downLoadLatestReleasePrestashop,
} from "../utils";
import { HiDocumentDownload } from "react-icons/hi";

export default function ModalForm({ gitUrl, version, isUrl, isPdf }) {
  const [openModal, setOpenModal] = useState(false);
  const [success, setSuccess] = useState(false);
  const [fail, setFail] = useState(false);
  const [loading, setLoading] = useState(false);

  const [formData, setFormData] = useState({
    firstName: "",
    emailAddress: "",
    website: "",
    phoneNumber: "",
    teams: "T66168ff398646933",
    description: gitUrl + " - " + version,
  });

  const isLocalEnvironment = () => {
    return (
      window.location.hostname === "localhost" ||
      window.location.hostname === "127.0.0.1"
    );
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    try {
      const formDataToSend = new FormData();

      const url = isLocalEnvironment()
        ? "http://localhost/send-lead-cetelem/?action=createLead"
        : "https://leadsender.dabasystem.com/?action=createLead";

      // Convertir el objeto formData a FormData
      Object.keys(formData).forEach((key) => {
        formDataToSend.append(key, formData[key]);
      });

      const response = await fetch(url, {
        method: "POST",
        body: formDataToSend,
        /* mode: "no-cors",  */ // Aquí se establece el modo no-cors
      });

      if (response.ok) {
        if (gitUrl.indexOf("prestashop") !== -1) {
          downLoadLatestReleasePrestashop(gitUrl);
        } else {
          downLoadLatestRelease(gitUrl);
        }

        setSuccess(true);
        setFail(false);
        setLoading(false);

        setTimeout(() => {
          setOpenModal(false);

          setFormData({
            firstName: "",
            emailAddress: "",
            website: "",
            phoneNumber: "",
            teams: "T66168ff398646933",
            description: gitUrl + " - " + version,
          });

          setSuccess(false);
          setFail(false);
        }, 3500);
      }
    } catch (error) {
      setSuccess(false);
      setFail(true);
      setLoading(false);
      console.error("Error al enviar los datos:", error);
    }
  };

  return (
    <>
      {isUrl ? (
        <a
          href={gitUrl}
          target="blank"
          color="white"
          className="inline-flex cursor-pointer items-center justify-center px-5 py-3 font-medium text-center text-gray-900 border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 dark:text-white dark:border-gray-700 dark:hover:bg-gray-700 dark:focus:ring-gray-800"
        >
          {`${version}`}
        </a>
      ) : isPdf ? (
        <a
          href={gitUrl}
          target="blank"
          color="white"
          className="text-sm cursor-pointer inline-flex items-center justify-center px-5 py-3 font-medium text-center text-gray-900 border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 dark:text-white dark:border-gray-700 dark:hover:bg-gray-700 dark:focus:ring-gray-800"
        >
          <HiDocumentDownload className="mr-2 h-5 w-5" />
          {`${version}`}
        </a>
      ) : (
        <>
          <div
            className="text-sm cursor-pointer inline-flex items-center justify-center px-5 py-3 font-medium text-center text-gray-900 border border-gray-300 rounded-lg hover:bg-gray-100 focus:ring-4 focus:ring-gray-100 dark:text-white dark:border-gray-700 dark:hover:bg-gray-700 dark:focus:ring-gray-800"
            onClick={() => setOpenModal(true)}
          >{`${version}`}</div>
          <Modal
            show={openModal}
            size="md"
            popup
            onClose={() => setOpenModal(false)}
            initialFocus="email"
          >
            <Modal.Header />
            <Modal.Body>
              <form onSubmit={handleSubmit}>
                <div className="space-y-6">
                  <h3 className="text-xl font-medium text-gray-900 dark:text-white">
                    Regístrate para obtener tu módulo gratis
                  </h3>
                  <div className=" flex flex-row gap-2">
                    <div>
                      <div className="mb-2 block">
                        <Label htmlFor="firstName" value="Nombre" />
                      </div>
                      <TextInput
                        id="firstName"
                        name="firstName"
                        value={formData.firstName}
                        onChange={handleInputChange}
                        required
                      />
                    </div>
                    <div>
                      <div className="mb-2 block">
                        <Label htmlFor="emailAddress" value="Email" />
                      </div>
                      <TextInput
                        id="emailAddress"
                        name="emailAddress"
                        value={formData.emailAddress}
                        onChange={handleInputChange}
                        placeholder="name@company.com"
                        required
                      />
                    </div>
                  </div>
                  <div>
                    <div className="mb-2 block">
                      <Label htmlFor="website" value="Sitio web" />
                    </div>
                    <TextInput
                      id="website"
                      name="website"
                      value={formData.website}
                      onChange={handleInputChange}
                      placeholder="https://www.ejemplo.com"
                    />
                  </div>
                  <div>
                    <div className="mb-2 block">
                      <Label htmlFor="phoneNumber" value="Teléfono" />
                    </div>
                    <TextInput
                      id="phoneNumber"
                      name="phoneNumber"
                      value={formData.phoneNumber}
                      onChange={handleInputChange}
                      placeholder="+34 650 322 254"
                    />
                  </div>
                  <div className="flex justify-between">
                    <div className="flex items-center gap-2">
                      <Checkbox id="accept" required />
                      <Label htmlFor="accept">
                        <a
                          href="https://www.cetelem.es/proteccion-de-datos"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          Acepto la política de privacidad y condiciones de uso
                        </a>
                      </Label>
                    </div>
                  </div>
                  {success && (
                    <p className=" text-green-600">
                      Gracias por confiar en Cetelem, tu descarga debería
                      empezar de inmediato.
                    </p>
                  )}
                  {fail && (
                    <p className=" text-red-500">
                      Algo ha ocurrido, revisa tus datos.
                    </p>
                  )}
                  <div className="w-full">
                    <Button
                      color="primary"
                      isProcessing={loading}
                      disabled={loading}
                      type="submit"
                    >
                      Enviar
                    </Button>
                  </div>
                </div>
              </form>
            </Modal.Body>
          </Modal>
        </>
      )}
    </>
  );
}
