import Header from "./components/Header";
import { Flowbite, Button } from "flowbite-react";
import { Hero } from "./components/Hero";
import { CtaSection } from "./components/CtaSection";
import { FooterMain } from "./components/FooterMain";

// https://flowbite-react.com/docs/getting-started/introduction
// https://flowbite.com/blocks/

const prestashop_git_url = [
  {
    url: "dabasystem-solutions/cetelem-prestashop-module-1.7.x",
    version: "Prestashop 1.7.x - 8.x",
  }
];

const magento_git_url = [
  {
    url: "dabasystem-solutions/cetelem-magento-module",
    version: "Magento 2.x",
  },
  /* {
    url: "/dummy.pdf",
    version: "Configurar plugin",
    isUrl: false,
    isPdf: true,
  }, */
];

const shopify_git_url = [
  {
    url: "https://apps.shopify.com/cetelem-calculadora?locale=es",
    version: "Calculadora Cetelem",
    isUrl: true,
  },
  {
    url: "https://apps.shopify.com/cetelem?locale=es",
    version: "Pasarela de pago Cetelem",
    isUrl: true,
  },
/*   {
    url: "/dummy.pdf",
    version: "Configurar plugin",
    isUrl: false,
    isPdf: true,
  }, */
];

const woocommerce_git_url = [
  {
    url: "/woocommerce/wc-ecredit-moto-payment-gateway-2.0.zip",
    version: "Ecredit moto",
    isUrl: true,
  },
  {
    url: "/woocommerce/woocommerce-cetelem-payment-gateway-3.0.zip",
    version: "Pasarela de pago Cetelem",
    isUrl: true,
  },

];

const customTheme = {
  button: {
    color: {
      primary: "bg-[#3A913F] hover:bg-[#154734] text-white",
    },
  },
};

function App() {
  return (
    <Flowbite theme={{ theme: customTheme }}>
      <Header></Header>
      <Hero></Hero>
      <CtaSection
        stylesSection="bg-gradient-to-r from-[#760f600f] dark:from-[#760f6057]"
        styles="max-w-[calc(100%-5%)] mx-auto sm:max-w-[calc(100%-19%)]"
        sectionId="prestashop-section"
        versions={prestashop_git_url}
        imagen="prestashop-big.jpg"
        titulo="Módulo de pago para PrestaShop"
        texto="Integra fácilmente los métodos de pago de CETELEM en tu tienda PrestaShop."
      ></CtaSection>
      <CtaSection
        stylesSection="bg-gradient-to-r from-[#eb642d1f] "
        styles="max-w-[calc(100%-5%)] mx-auto sm:max-w-[calc(100%-19%)]"
        sectionId="magento-section"
        versions={magento_git_url}
        imagen="magento-big.png"
        titulo="Módulo de pago para Magento"
        texto="Facilita el proceso de pago para tus clientes con nuestro módulo de pago compatible con Magento."
      ></CtaSection>
      <CtaSection
        stylesSection="bg-gradient-to-r from-[#90b9451f]"
        styles="max-w-[calc(100%-5%)] mx-auto sm:max-w-[calc(100%-19%)]"
        sectionId="shopify-section"
        versions={shopify_git_url}
        imagen="shopify-big.webp"
        titulo="Módulo de pago para Shopify"
        texto="Descubre nuestra integración perfecta con Shopify para ofrecer una experiencia de pago sin contratiempos."
      ></CtaSection>
      <CtaSection
        stylesSection="bg-gradient-to-r from-[#8053b424]"
        styles="max-w-[calc(100%-5%)] mx-auto sm:max-w-[calc(100%-19%)]"
        sectionId="woocommerce-section"
        versions={woocommerce_git_url}
        imagen="WooCommerce_logo.png"
        titulo="Módulo de pago para Woocommerce"
        texto="Explora nuestra integración fluida con Woocommerce para proporcionar una experiencia de pago sin inconvenientes."
      ></CtaSection>

      <FooterMain></FooterMain>
    </Flowbite>
  );
}

export default App;
